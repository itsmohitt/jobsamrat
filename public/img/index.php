<!DOCTYPE html>
<html lang="en">
<?php require_once 'dbConnect.php'; ?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Job Samrat | Latest Job Notifications | Admit Cards | Result Links | Goverment Jobs</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php include 'header.php'; ?>
    

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Welcome to Job Samrat
                </h1>
            </div>
            <div class="col-lg-12">
            <?php 
            $today=date('Y-m-d');
            $morQuery="SELECT * FROM jobsnoti WHERE ((JobLastDate>='$today' AND JobImp='1') OR (JobAdmitLastDate>='$today' AND JobAdmitImp='1') OR (JobResultImp='1')) AND JobStatus='1' ORDER BY JobPost DESC LIMIT 15"; 
            $morResult=mysqli_query($connect,$morQuery);
            ?>
            <marquee onmouseover="this.stop()" onmouseout="this.start()"><?php while($morRow=mysqli_fetch_assoc($morResult)){
	            	if($morRow['JobResultImp']){
	            		echo '<a href="job.php?jobID='.$morRow['JobId'].'" class="lead">| RESULT : '.$morRow['JobShortName'].'</a>';
	            	}elseif($morRow['JobAdmitImp']){
	            		echo '<a href="job.php?jobID='.$morRow['JobId'].'" class="lead">| ADMIT CARD : '.$morRow['JobShortName'].'</a>';
	            	}elseif($morRow['JobImp']){
	            		echo '<a href="job.php?jobID='.$morRow['JobId'].'" class="lead" data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">| '.$morRow['JobShortName'].'</a>';
	            	}
            	} ?></marquee>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-tasks"></i> Latest Jobs  Notifications</h4>
                    </div>
                    <div class="panel-body">

                    	<?php 
                    	
                    	$jobQuery="SELECT * FROM jobsnoti WHERE JobLastDate>='$today' AND JobStatus='1'";
                    	$jobResult=mysqli_query($connect,$jobQuery);
                    	while($jobRow=mysqli_fetch_assoc($jobResult)){
                    		echo '<p><a href="job.php?jobID='.$jobRow['JobId'].'">'.$jobRow['JobTitle'].'</a><br><small>Last Date: '.date('d-m-Y',strtotime($jobRow['JobLastDate'])).'</small></p>';
                    	}
                    	?>
                        <a href="#" class="btn btn-default">More Jobs</a>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-clipboard"></i> Results</h4>
                    </div>
                    <div class="panel-body">
                    <?php
                    	
                        $morQuery="SELECT * FROM jobsnoti WHERE JobResult='1' AND JobStatus='1'  ORDER BY JobPost DESC LIMIT 15"; 
			            $morResult=mysqli_query($connect,$morQuery);
			            while($morRow=mysqli_fetch_assoc($morResult)){
				            
			            		echo '<p><a href="job.php?jobID='.$morRow['JobId'].'" data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">RESULT : '.$morRow['JobShortName'].'</a></p>';
			            	
		            	} ?>
                    	<a href="#" class="btn btn-default">More Results</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Important</h4>
                    </div>
                    <div class="panel-body">
                    <?php
                    	$lessDays=date('Y-m-d',strtotime($today .'+ 15 days'));
                        $morQuery="SELECT * FROM jobsnoti WHERE (JobLastDate<='$lessDays' AND JobLastDate>='$today') OR (JobAdmitLastDate<='$lessDays' AND JobAdmitLastDate>='$today') AND JobStatus='1' ORDER BY JobPost DESC LIMIT 15"; 
			            $morResult=mysqli_query($connect,$morQuery);
			            while($morRow=mysqli_fetch_assoc($morResult)){
				            if($morRow['JobResult']=='1'){
			            		echo '<p><a href="job.php?jobID='.$morRow['JobId'].'" data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">RESULT : '.$morRow['JobShortName'].'</a></p>';
			            	}elseif($morRow['JobAdmitCard']=='1'){
			            		echo '<p><a href="job.php?jobID='.$morRow['JobId'].'"data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">ADMIT CARD : '.$morRow['JobShortName'].'</a><br><small>Last Date: '.date('d-m-Y',strtotime($morRow['JobAdmitLastDate'])).'</small></p>';
			            	}else{
			            		echo '<p><a href="job.php?jobID='.$morRow['JobId'].'" data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">'.$morRow['JobShortName'].'</a><br><small>Last Date: '.date('d-m-Y',strtotime($morRow['JobLastDate'])).'</small></p>';
			            	}
		            	} ?>
		            	<a href="#" class="btn btn-default">More Important</a>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-credit-card"></i> Admit Cards</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                    	
                        $morQuery="SELECT * FROM jobsnoti WHERE JobAdmitCard='1' AND JobStatus='1' ORDER BY JobPost DESC LIMIT 15"; 
			            $morResult=mysqli_query($connect,$morQuery);
			            while($morRow=mysqli_fetch_assoc($morResult)){
				            
			            		echo '<p><a href="job.php?jobID='.$morRow['JobId'].'" data-toggle="tooltip" data-placement="top" title="'.$morRow['JobTitle'].'">ADMIT CARD : '.$morRow['JobShortName'].'</a><br><small>Last Date: '.date('d-m-Y',strtotime($morRow['JobAdmitLastDate'])).'</small></p>';
			            	
		            	} ?><a href="#" class="btn btn-default">More Admit Cards</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Exam Preparation</h4>
                    </div>
                    <div class="panel-body">
                    	<?php
                    		$topicQuery="SELECT TopicName,TopicId FROM quetopic WHERE TopicStatus='1' AND TopicStatus='1'LIMIT 15";
                    		$topicResult=mysqli_query($connect,$topicQuery);
                    		while($topicRow=mysqli_fetch_assoc($topicResult)){
                    			echo '<h4 class="examTopics"><i class="fa fa-arrows"></i> '.$topicRow['TopicName'].'</h4><ul class="examSubTopics " style="display:none">';
                    			$subTopicQuery="SELECT SubTopicName,SubTopicId FROM quesubtopic WHERE TopicId='$topicRow[TopicId]' AND SubTopicStatus='1' LIMIT 10";
                    			$subTopicResult=mysqli_query($connect,$subTopicQuery);
                    			while($subTopicRow=mysqli_fetch_assoc($subTopicResult)){
                    				echo '<li><a href="examSubTopics.php?subtopicID='.$subTopicRow['SubTopicId'].'">'.$subTopicRow['SubTopicName'].'</a></li>';
                    			}
                                echo '<a class="btn btn-xs btn-success" href="examTopics.php?topicID='.$topicRow['TopicId'].'">More Topics</a>';
                                echo '</ul>';
                    		}
                    	?>
                        
                    	<a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- /.row -->

       <hr>


       

       

    </div>
    <!-- /.container -->
    <?php include 'footer.php'; ?>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

   
    <?php include 'scripts.php'; ?>
</body>
<script>
jQuery('.examTopics').click(function(){
    
    jQuery('ul.examSubTopics').hide('slow');
    jQuery(this).next('ul.examSubTopics').show('slow');
});

</script>
</html>
