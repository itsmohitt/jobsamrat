<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Mohit Gupta',
            'userType'=> 'USER',
            'userImg'=>'user.jpg',
            'email' => 'itsmohitt@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'ADMIN',
            'userType'=> 'ADMIN',
            'userImg'=>'user.jpg',
            'email' => 'info@jobsamrat.com',
            'password' => bcrypt('admin123'),
        ]);
    }
}
