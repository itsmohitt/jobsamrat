<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

Route::get('/', 'job@home');
Route::get('about',function(){
    return view('about');
});
Route::get('contact',function(){
        return view('contact');
});
Route::get('faq',function(){
        return view('faq');
});
//Job Routes
Route::get('job','job@jobs');
Route::get('job/latest','job@latest');
Route::get('job/last','job@last');
Route::post('job/{id}/comment','HomeController@comment');
Route::get('job/{id}/{name?}','job@jobDetail');

//Forum Routes
Route::group(['prefix'=>'forum'],function(){
        Route::get('/','forum@home');
        Route::post('/add','forum@createTopic');
        Route::get('/overview','forum@overview');
        Route::post('/overview','forum@searchOverview');
        Route::get('/overview/{id}/{name?}','forum@catOverview');
        Route::get('/search','forum@home');
        Route::get('/{id}/{name?}','forum@forumTopic');

});

//Question Routes
Route::get('examPrep','jobQues@examPrep');
Route::get('topic/{id}/{name?}','jobQues@queTopic');
Route::get('subTopic/{id}/{name?}','jobQues@quesubTopic');
Route::get('question/{id}','jobQues@queDetail');
Route::post('question/{id}/comment','HomeController@comment');

View::composer('header',function($view){
    return $view->with('var','value');
});

//Normal User Routes

Route::get('/home', 'HomeController@index');
Route::get('/profile','HomeController@profile');

//Admin Routes
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware','prefix'=>'/admin/settings'],function(){

        Route::get('/', 'AdminController@adminHome');  //Admin Dashboard

        //Job Manage Routes
        Route::get('/jobs', 'job@jobs');
        Route::get('/jobs/create', 'AdminController@createJob');
        Route::post('/jobs/create', 'AdminController@storeJob');
        Route::get('/jobs/edit/{id}', 'AdminController@editJob');
        Route::post('/jobs/edit/{id}', 'AdminController@updateJob');

        //Question Manage Routes
        Route::get('/questions', 'AdminController@questions');
        Route::get('/questions/create', 'AdminController@createQue');
        Route::post('/questions/create', 'AdminController@storeQue');
        Route::get('/questions/edit/{id}', 'AdminController@editQue');
        Route::post('/questions/edit/{id}', 'AdminController@updateQue');

        //Category Settings
        Route::get('/category','AdminController@category');
        Route::post('/category/add','AdminController@categoryAdd');
        Route::post('/category/update/{id}','AdminController@categoryUpdate');
        Route::post('/category/delete','AdminController@categoryDelete');
        Route::post('/subcategory/add','AdminController@subCategoryAdd');
        Route::post('/subcategory/update/{id}','AdminController@subCategoryUpdate');
        Route::post('/subcategory/delete','AdminController@subCategoryDelete');
});