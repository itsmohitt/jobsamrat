@extends('main')
@section('headContent')
    <title>Frequently Asked Questions | Job Samrat</title>
@endsection
@section('pageName')
    <h2>Frequently Asked Questions (FAQ)</h2>
@endsection
@section('bodyContent')
    <section id="sp-main-body">
        <div class="container">
            <div class="row">
                <div id="sp-component" class="col-sm-12 col-md-12">
                    <div class="sp-column ">
                        <div id="system-message-container">
                        </div>
                        <article class="item item-page" itemscope itemtype="http://schema.org/Article">
                            <meta itemprop="inLanguage" content="en-GB"/>


                            <div class="entry-header">


                            </div>


                            <div itemprop="articleBody">
                                <div class="sprocket-lists" data-lists="108">
                                    <ul class="sprocket-lists-container" data-lists-items>
                                        <li class="active" data-lists-item>
                                            <h4 class="sprocket-lists-title padding" data-lists-toggler>
                                                How to fill my tax declaration? <span class="indicator"></span></h4>
							<span class="sprocket-lists-item" data-lists-content>
								<span class="sprocket-padding">
									At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.			<br>
									<a href="#" class="readon"></a>
								</span>
							</span>
                                        </li>
                                        <li data-lists-item>
                                            <h4 class="sprocket-lists-title padding" data-lists-toggler>
                                                What documents I need for audit? <span class="indicator"></span></h4>
								<span class="sprocket-lists-item" data-lists-content>
									<span class="sprocket-padding">
										At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.			<br>
										<a href="#" class="readon"></a>
									</span>
								</span>
                                        </li>
                                        <li data-lists-item>
                                            <h4 class="sprocket-lists-title padding" data-lists-toggler>
                                                How to manage my finances? <span class="indicator"></span></h4>
									<span class="sprocket-lists-item" data-lists-content>
										<span class="sprocket-padding">
											At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.			<br>
											<a href="#" class="readon"></a>
										</span>
									</span>
                                        </li>
                                        <li data-lists-item>
                                            <h4 class="sprocket-lists-title padding" data-lists-toggler>
                                                What is the 50/30/20 rule? <span class="indicator"></span></h4>
										<span class="sprocket-lists-item" data-lists-content>
											<span class="sprocket-padding">
												At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.			<br>
												<a href="#" class="readon"></a>
											</span>
										</span>
                                        </li>
                                        <li data-lists-item>
                                            <h4 class="sprocket-lists-title padding" data-lists-toggler>
                                                Do you offer bookkeeping training? <span class="indicator"></span></h4>
											<span class="sprocket-lists-item" data-lists-content>
												<span class="sprocket-padding">
													At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia.			<br>
													<a href="#" class="readon"></a>
												</span>
											</span>
                                        </li>
                                    </ul>
                                    <div class="sprocket-lists-nav">
                                        <div class="sprocket-lists-pagination-hidden">
                                            <ul>
                                                <li class="active" data-lists-page="1"><span>1</span></li>
                                            </ul>
                                        </div>
                                        <div class="spinner"></div>
                                    </div>
                                </div>
                            </div>


                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection