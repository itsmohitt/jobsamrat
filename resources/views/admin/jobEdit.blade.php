@extends('main')
@section('scriptContent')
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script>CKEDITOR.replace( 'jobDescr' );</script>
@endsection
@section('bodyContent')
    <section id="sp-top-a">
        <!-- Page Content -->
        <div class="container">
            <!-- Intro Content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary division" id="addJobs">
                        <div class="panel-heading">
                            <h4>Edit Jobs</h4>
                        </div>
                        <div class="panel-body">
                            <form action="#" method="post" >
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Job Headline</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="jobTitle" value="{!! $job->JobTitle !!}" placeholder="Headline" maxlength="255" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Job Short Name</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" value="{!! $job->JobShortName !!}" name="jobShortTitle" maxlength="50">
                                        </div>
                                        <label class="control-label col-md-3">Breaking Job
                                        </label>
                                        <div class="col-md-3">
                                            <div class="col-md-4">
                                                <input type="checkbox" name="jobImp" class="form-control" value="YES"{!! ($job->JobImp=='1'?'checked':'') !!}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Job Form Last Date</label>
                                        <div class="col-md-3">
                                            <input type="date" class="form-control" name="jobLastDate" value="{!! $job->JobLastDate !!}" maxlength="50"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Description</label>
                                        <div class="col-md-9">
                                            <textarea name="jobShortDescr" class="form-control">{!! $job->JobShortDescr !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Tags</label>
                                        <div class="col-md-9">
                                            <input name="jobTags" id="tags" class="form-control" value="{!! $job->JobTags !!}"/>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Description</label>
                                        <div class="col-md-12">
                                            <textarea id="wysiwyg" placeholder="Enter text here..." name="jobDescr"
                                                      class="form-control" rows="10">{!! $job->JobDescr !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-primary btn-block" value="UPDATE JOB">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </section>
@endsection