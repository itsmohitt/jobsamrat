<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/13/2016
 * Time: 10:04 PM
 */ ?>
@extends('main')
@section('headContent')
    <title>Admin Panel | Job Samrat</title>
@stop
@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">
            <div class="row">
                <div id="sp-top3" class="col-sm-12 col-md-12">
                    <div class="sp-column ">
                        <div class="sp-module title1"><h3 class="sp-module-title">Admin Panel</h3>
                            <div class="sp-module-content">
                                <div class="sprocket-strips-p" data-strips="91">
                                    <div class="sprocket-strips-p-overlay">
                                        <div class="css-loader-wrapper">
                                            <div class="css-loader"></div>
                                        </div>
                                    </div>
                                    <ul class="sprocket-strips-p-container cols-4" data-strips-items>
                                        <li class="sprocket-strips-p-block" data-strips-item>
                                            <div class="sprocket-strips-p-item" data-strips-content>
                                                <div class="sprocket-strips-p-content">
                                                    <div class="sprocket-strips-p-text">
                                                        <div class="conto-box"><a href="{!! URL::to('admin/settings/jobs') !!}"> <span class="conto-icon"><i
                                                                            class="uk-icon-black-tie"></i></span>
                                                                <div class="conto-content"><h4 class="conto-main">
                                                                        Jobs</h4>
                                                                    <p class="conto-desc">Add New Job<br>
                                                                    Update Job<br>
                                                                    Delete Job</p></div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sprocket-strips-p-block" data-strips-item>
                                            <div class="sprocket-strips-p-item" data-strips-content>
                                                <div class="sprocket-strips-p-content">
                                                    <div class="sprocket-strips-p-text">
                                                        <div class="conto-box"><a href="{!! URL::to('/admin/settings/category') !!}"> <span class="conto-icon"><i
                                                                            class="uk-icon-folder-open-o"></i></span>
                                                                <div class="conto-content"><h4 class="conto-main">
                                                                        Question Categories</h4>
                                                                    <p class="conto-desc">Add Category<br>
                                                                    Update/Delete Category<br><br>
                                                                    Add Sub-Category<br>
                                                                    Update/Delete Sub-Category</p></div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sprocket-strips-p-block" data-strips-item>
                                            <div class="sprocket-strips-p-item" data-strips-content>
                                                <div class="sprocket-strips-p-content">
                                                    <div class="sprocket-strips-p-text">
                                                        <div class="conto-box"><a href="{!! URL::to('admin/settings/questions') !!}"> <span class="conto-icon"><i
                                                                            class="uk-icon-money"></i></span>
                                                                <div class="conto-content"><h4 class="conto-main">
                                                                        Exam Questions</h4>
                                                                    <p class="conto-desc">Add New Question<br>
                                                                    Update Question<br>
                                                                    Delete Question</p></div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="sprocket-strips-p-block" data-strips-item>
                                            <div class="sprocket-strips-p-item" data-strips-content>
                                                <div class="sprocket-strips-p-content">
                                                    <div class="sprocket-strips-p-text">
                                                        <div class="conto-box"><a href="#"> <span class="conto-icon"><i
                                                                            class="uk-icon-support"></i></span>
                                                                <div class="conto-content"><h4 class="conto-main">
                                                                        Quizes</h4>

                                                                    <p class="conto-desc">Wait for the upgrades</p></div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="sprocket-strips-p-nav">
                                        <div class="sprocket-strips-p-pagination-hidden">
                                            <ul>
                                                <li class="active" data-strips-page="1"><span>1</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
