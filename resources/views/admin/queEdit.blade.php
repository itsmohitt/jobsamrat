@extends('main')

@section('headContent')
    <title>Edit Question</title>
@endsection
@section('pageName')
    <h2>Edit Question</h2>
@endsection

@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">
            <div class="row">
                <div class="panel panel-primary division" id="addQues">
                    <div class="panel-heading">
                        <h4>Edit Question</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="#" enctype='multipart/form-data'>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Question</label>
                                    <div class="col-md-9">
                                        <textarea name="queTitle" value="{!! $que->Question !!}" class="form-control" placeholder="Write Question..." maxlength="5000" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option A</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" value="{!! $que->OptionA !!}" name="optionA" maxlength="100" required>
                                    </div>
                                    <label class="control-label col-md-3">Option B</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" value="{!! $que->OptionB !!}" name="optionB" maxlength="100" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option C</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" value="{!! $que->OptionC !!}" name="optionC" maxlength="100" required>
                                    </div>
                                    <label class="control-label col-md-3">Option D</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" value="{!! $que->OptionD !!}" name="optionD" maxlength="100" required>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option E</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" value="{!! $que->OptionE !!}" name="optionE" maxlength="100">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Answer</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="queAnswer" required>
                                            <option>Choose One</option>
                                            <option value="A" {!! ($que->QueAnswer == "A"?'selected':'') !!}>Option A</option>
                                            <option value="B" {!! ($que->QueAnswer == "B"?'selected':'') !!}>Option B</option>
                                            <option value="C" {!! ($que->QueAnswer == "C"?'selected':'') !!}>Option C</option>
                                            <option value="D" {!! ($que->QueAnswer == "D"?'selected':'') !!}>Option D</option>
                                            <option value="E" {!! ($que->QueAnswer == "E"?'selected':'') !!}>Option E</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-3">Difficulty</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="queDifficulty" required>
                                            <option>Choose One</option>
                                            <option value="1" {!! ($que->QueDifficulty == "1"?'selected':'') !!}>Level 1</option>
                                            <option value="2" {!! ($que->QueDifficulty == "2"?'selected':'') !!}>Level 2</option>
                                            <option value="3" {!! ($que->QueDifficulty == "3"?'selected':'') !!}>Level 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Category</label>
                                    <div class="col-md-9">
                                        <select class="select2" name="subTopic[]" multiple data-placeholder="Choose Topics" required>
                                            @foreach($subTopics as $subTopic)
                                                <option value="{!! $subTopic->SubTopicId !!}" {!! ($que->QueDifficulty == "1"?'selected':'') !!}>{!! $subTopic->SubTopicName !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            {!! csrf_field() !!}
                            <input type="submit" name="quesAddSubmit" class="btn btn-primary btn-block"
                                   value="ADD QUESTION">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection