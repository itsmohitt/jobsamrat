<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/14/2016
 * Time: 12:42 AM
 */?>
@extends('main')
@section('headContent')
    <title>Question</title>
@endsection
@section('bodyContent')
    <section id="sp-top-a">
        <div id="fb-root"></div>

        <!-- Page Content -->
        <div class="container">

            <!-- Intro Content -->
            <div class="row">
                <div class="col-md-9">
                    <div class="panel panel-success">
                        <div class="panel-body">
                        <?php $i=0 ?>
                        @foreach($questions as $question)
                                <?php $i++ ?>
                            <p>Q. {!! $question->Question !!}<br></p>




                                <div class="row">
                                    <div class="col-md-6">A) <b>{!! $question->OptionA !!}</b></div>
                                    <div class="col-md-6">B) <b>{!! $question->OptionB !!}</b></div>
                                    <div class="col-md-6">C) <b>{!! $question->OptionC !!}</b></div>
                                    <div class="col-md-6">D) <b>{!! $question->OptionD !!}</b></div>
                                    <div class="col-md-6">E) <b>{!! $question->OptionE !!}</b></div>
                                </div>

                                <div class="row">
                                    <div class="pull-right col-sm-12 col-md-3 col-xs-12">

                                        <div class="progress">
                                            <div class="progress-bar progress-bar-{!! ($question->QueDifficulty=='3'?'danger':($question->QueDifficulty=='2'?'warning':'success')) !!}" role="progressbar" aria-valuenow="70"
                                                 aria-valuemin="0" aria-valuemax="100" style="width:{!! ($question->QueDifficulty=='3'?'100':($question->QueDifficulty=='2'?'67':'34')) !!}%">
                                                <small> LEVEL : {!! $question->QueDifficulty !!}</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-xs-12 col-sm-12" >
                                        <div class="btn-group">
                                        <a type="button" class="btn btn-sm btn-success showAnswer" ><i class="fa fa-pencil" alt="Explanation" ></i> Show Answer</a>
                                        <a type="button" href="{!! URL::to('question/'.$question->QueId) !!}" title="Explanation" class="btn btn-sm btn-success"><i class="fa fa-book" alt="Explanation" ></i> Explanation</a>
                                        <a type="button" href="{!! URL::to('question/'.$question->QueId.'#comments') !!}" title="Comments" class="btn btn-sm btn-success"><i class="fa fa-commenting-o " ></i> Comments</a>
                                        <a type="button" href="#" title="Share" class="btn btn-sm btn-success"><i class="fa fa-share " ></i> Share</a>
                                        <a type="button" href="#" title="Report" class="btn btn-sm btn-success"><i class="fa fa-exclamation-circle" ></i> Report</a>
                                        </div>

                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <p class="bg-primary answerText" style="display:none">&nbsp;&nbsp;Correct Answer :&nbsp;{!!  $question->QueAnswer !!}</p>
                                    </div>
                                </div>

                            <hr>
                        @endforeach
                            @if($i==0)
                                <h2>Ooops!!</h2>
                                <p class="lead">We are continously working at new question and keep updating them, we still regret for the problem.<br>Dont worry,we will update soon. Keep visiting us.</p>
                            @endif
                        {!! $questions->links() !!}
                    </div>
                        </div>
                   </div>
                <div class="col-md-3 pull-right">
                    <script>
                        (function() {
                            var cx = '000664330352881482014:irrwudbxgf0';
                            var gcse = document.createElement('script');
                            gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s);
                        })();
                    </script>
                    <gcse:search></gcse:search>
                    <br>
                    <div data-WRID="WRID-147844527591248304" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('scriptContent')
    <script>
        jQuery('a.showAnswer').click(function(){

            jQuery(this).parents('div.row').children('.col-md-12').children('p.answerText').show('slow');

        });
    </script>
@endsection