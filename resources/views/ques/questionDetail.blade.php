<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/14/2016
 * Time: 12:42 AM
 */
$question=$var[0];
$subTopics=$var[1];
$comments=$var[2];
?>
@extends('main')
@section('headContent')
    <title>Question</title>
@endsection
@section('bodyContent')
    <section id="sp-top-a">
        <div id="fb-root"></div>

        <!-- Page Content -->
        <div class="container">

            <!-- Intro Content -->
            <div class="row">
                <div class="col-md-9">


                                <p class="lead">Q. {!! $question->Question !!}<br></p>

                                <div class="row">
                                    <div class="col-md-6">A) <b>{!! $question->OptionA !!}</b></div>
                                    <div class="col-md-6">B) <b>{!! $question->OptionB !!}</b></div>
                                    <div class="col-md-6">C) <b>{!! $question->OptionC !!}</b></div>
                                    <div class="col-md-6">D) <b>{!! $question->OptionD !!}</b></div>
                                    <div class="col-md-6">E) <b>{!! $question->OptionE !!}</b></div>

                                </div>
                    <br>
                    <div class="row">
                        <div class="pull-right col-sm-12 col-md-3 col-xs-12">

                            <div class="progress">
                                <div class="progress-bar progress-bar-{!! ($question->QueDifficulty=='3'?'danger':($question->QueDifficulty=='2'?'warning':'success')) !!}" role="progressbar" aria-valuenow="70"
                                     aria-valuemin="0" aria-valuemax="100" style="width:{!! ($question->QueDifficulty=='3'?'100':($question->QueDifficulty=='2'?'67':'34')) !!}%">
                                    <small> LEVEL : {!! $question->QueDifficulty !!}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-xs-12 col-sm-12" >
                            <div class="btn-group">

                                <a type="button" href="{!! URL::to('question/'.$question->QueId) !!}" title="Explanation" class="btn btn-sm btn-success"><i class="fa fa-book" alt="Explanation" ></i> Explanation</a>
                                <a type="button" href="{!! URL::to('question/'.$question->QueId.'#comments') !!}" title="Comments" class="btn btn-sm btn-success"><i class="fa fa-commenting-o " ></i> Comments</a>
                                <a type="button" href="#" title="Share" class="btn btn-sm btn-success"><i class="fa fa-share " ></i> Share</a>
                                <a type="button" href="#" title="Report" class="btn btn-sm btn-success"><i class="fa fa-exclamation-circle" ></i> Report</a>
                            </div>

                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <p class="bg-primary answerText">&nbsp;&nbsp;Correct Answer :&nbsp;{!!  $question->QueAnswer !!}</p>
                        </div>
                    </div>
                                <hr>
                            <div class="row" id="comments">
                                <!-- Comments Form -->
                                <div class="well">
                                    <h4>Leave a Comment:</h4>
                                    <form role="form" action="{!! URL::to('question/'.$question->QueId.'/comment') !!}" method="post">
                                        <div class="form-group">
                                            <textarea class="form-control" name="comment" rows="3"></textarea>
                                        </div>
                                        <input type="hidden" name="type" value="Q">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>

                                <hr>

                                <!-- Posted Comments -->

                                <!-- Comment -->
                                @foreach($comments as $comment)
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" src="{!! URL::to('images/users/user.jpg') !!}"  width="70px" alt="">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">{!! $comment->name !!}
                                            <small>{!! date('d M Y, h:i A',strtotime($comment->updated_at)) !!}</small>
                                        </h4>
                                        {!! $comment->Comment !!}
                                    </div>
                                </div>
                                @endforeach

                                <!-- Comment -->

                            </div>
                        </div>

                <div class="col-md-3 pull-right">

                    <script>
                        (function() {
                            var cx = '000664330352881482014:irrwudbxgf0';
                            var gcse = document.createElement('script');
                            gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s);
                        })();
                    </script>
                    <gcse:search></gcse:search>
                    <br>
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <h4>Tagged Topics</h4>
                            @foreach($subTopics as $subTopic)
                            <p><a href="{!! URL::to('subTopic/'.$subTopic->SubTopicId) !!}">{!! $subTopic->SubTopicName !!}</a></p>
                                @endforeach
                        </div>
                    </div>
                    <br>
                    <div data-WRID="WRID-147844527591248304" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('scriptContent')
    <script>
        jQuery('a.showAnswer').click(function(){

            jQuery(this).parents('div.row').children('.col-md-12').children('p.answerText').show('slow');

        });
    </script>
@endsection