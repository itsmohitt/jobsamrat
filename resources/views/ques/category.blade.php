@extends('main')
@section('headContent')
    <title>Category and Sub-Category Settings | Admin Panel | JobSamrat</title>
@endsection

@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">
            <div class="row">
                @if(Auth::guest())
                @elseif(Auth::user()->userType == 'ADMIN')
                <a class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#addCategory"><i
                            class="fa fa-plus"></i> Add New Category</a>
                <hr>
                @endif
                @foreach($topics as $topic)
                    <div class="col-sm-6 col-md-3 col-xs-12" id="section{!! $topic[0]->TopicId !!}">
                        <h1 >{!! $topic[0]->TopicName !!}</h1>
                        @if(Auth::guest())
                        @elseif(Auth::user()->userType == 'ADMIN')
                        <div class="btn-group" id="btnGroup{!! $topic[0]->TopicId !!}">
                            <a class="btn btn-info " data-toggle="modal" data-target="#editCategory" onclick="updateCat('{!! $topic[0]->TopicId !!}','{!! $topic[0]->TopicName !!}')"><i class="fa fa-pencil"></i> Edit</a>
                            <a class="btn btn-danger" onclick="deleteCat('{!! $topic[0]->TopicId !!}','{!! $topic[0]->TopicName !!}')"><i class="fa fa-times"></i> Delete</a>
                        </div>
                        <hr>
                        @endif

                        <ul class="list-group" style="height: 300px;overflow-y: scroll;">
                            @foreach($topic[1] as $subTopic)
                                <li class="list-group-item " id="list{!! $subTopic->SubTopicId !!}">
                                    @if(Auth::guest())
                                    @elseif(Auth::user()->userType == 'ADMIN')
                                    <div class="btn-group pull-right" id="subBtnGroup{!! $subTopic->SubTopicId !!}">
                                        <a class="badge btn-info" data-toggle="modal" data-target="#editSubCategory" onclick="updateSubCat('{!! $topic[0]->TopicId !!}','{!! $subTopic->SubTopicId !!}','{!! $subTopic->SubTopicName !!}')"><i class="fa fa-pencil"></i></a>
                                        <a class="badge btn-danger" onclick="deleteSubCat('{!! $subTopic->SubTopicId !!}','{!! $subTopic->SubTopicName !!}')"><i class="fa fa-times"></i></a>
                                    </div>
                                    @endif
                                   <a href="{!! URL::to('/subTopic/'.$subTopic->SubTopicId.'/'.str_replace(' ','-',$subTopic->SubTopicName)) !!}" > {!! $subTopic->SubTopicName !!} </a>

                                </li>
                            @endforeach
                        </ul>
                        @if(Auth::guest())
                        @elseif(Auth::user()->userType == 'ADMIN')
                        <a class="btn btn-primary btn-block" data-toggle="modal" data-target="#addSubCategory" onclick="addSubCat('{!! $topic[0]->TopicId !!}')"><i class="fa fa-plus"></i> Add Sub-Category</a>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        </div>
    </section>
    @if(Auth::guest())
    @elseif(Auth::user()->userType == 'ADMIN')
    <div id="addCategory" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <form class="form-horizontal" method="post" action="{!! URL::to('/admin/settings/category/add') !!}">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Category</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class=" col-md-4 control-label">Category Name</label>
                            <div class="col-md-8 ">
                                <input type="text" class="form-control" name="category" placeholder="Enter Category Name.." required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add Category</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div id="editCategory" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <form class="form-horizontal" id="editCatForm" method="post" action="{!! URL::to('/admin/settings/category/add') !!}">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <label class=" col-md-4 control-label">Category Name</label>
                                <div class="col-md-8 ">
                                    <input type="text" class="form-control" name="category" id="editCatName" placeholder="Enter Category Name.." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-info"><i class="fa fa-pencil"></i> Update Category</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="addSubCategory" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <form class="form-horizontal" id="editCatForm" method="post" action="{!! URL::to('/admin/settings/subcategory/add') !!}">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Sub Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <label class=" col-md-4 control-label">Category Name</label>
                                <div class="col-md-8 ">
                                    <select class="form-control" name="category" id="selectCategory">
                                        @foreach($topics as $topic)
                                            <option value="{!! $topic[0]->TopicId !!}">{!! $topic[0]->TopicName  !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-md-4 control-label">Sub Category Name</label>
                                <div class="col-md-8 ">
                                    <input type="text" class="form-control" name="subCategory"  placeholder="Enter Sub Category Name.." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add Sub Category</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="editSubCategory" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <form class="form-horizontal" id="editSubCatForm" method="post" action="{!! URL::to('/admin/settings/subcategory/add') !!}">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Sub Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <label class=" col-md-4 control-label">Category Name</label>
                                <div class="col-md-8 ">
                                    <select class="form-control" name="category" id="selectUpdateCategory">
                                        @foreach($topics as $topic)
                                            <option value="{!! $topic[0]->TopicId !!}">{!! $topic[0]->TopicName  !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" col-md-4 control-label">Sub Category Name</label>
                                <div class="col-md-8 ">
                                    <input type="text" class="form-control" name="subCategory" id="editSubCatName" placeholder="Enter Sub Category Name.." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-info"><i class="fa fa-pencil"></i> Update Sub Category</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection
@section('scriptContent')
    @if(Auth::guest())
    @elseif(Auth::user()->userType == 'ADMIN')
    <script>
        function updateCat(id,name){
            jQuery('#editCatName').val(name);
            jQuery('#editCatForm').attr('action','{!! URL::to('/admin/settings/category/update') !!}'+'/'+id);
            jQuery('#editCatName').focus();
        }
        function deleteCat(id,name){
            var confirmation = confirm("Are you sure to delete "+ name + " ? \nWARNING : All the questions and sub-categories under will also be deleted.");
            if(confirmation == true){
                jQuery.ajax({
                    url : '{!! URL::to('/admin/settings/category/delete/') !!}',
                    method : "POST",
                    beforeSend : function(){
                        jQuery("#btnGroup"+id).children().attr('disabled','disabled');
                    },
                    data : {
                        '_token'    : '{!! csrf_token() !!}',
                        'topic'  : id,
                    }
                }).done(function(msg){
                   jQuery('#section'+id).hide('slow');

                }).fail(function(msg){
                    console.log(msg.responseText);
                    jQuery("#btnGroup"+id).children().removeAttr('disabled');
                });

            }
        }
        function addSubCat(id){
            jQuery('#selectCategory').val(id);
        }
        function updateSubCat(id,subId,subName){
            jQuery('#selectUpdateCategory').val(id);
            jQuery('#editSubCatName').val(subName);
            jQuery('#editSubCatForm').attr('action','{!! URL::to('/admin/settings/subcategory/update') !!}'+'/'+subId);

        }
        function deleteSubCat(id,name){
            var confirmation = confirm("Are you sure to delete "+ name + " ? \nWARNING : All the questions under will also be deleted.");
            if(confirmation == true){
                jQuery.ajax({
                    url : '{!! URL::to('/admin/settings/subcategory/delete/') !!}',
                    method : "POST",
                    beforeSend : function(){
                        jQuery('#subBtnGroup'+id).hide('slow');
                    },
                    data : {
                        '_token'    : '{!! csrf_token() !!}',
                        'subTopic'  : id,
                    }
                }).done(function(msg){
                    jQuery('#list'+id).hide('slow');

                }).fail(function(msg){
                    console.log(msg.responseText);
                    jQuery('#subBtnGroup'+id).show('slow');
                });

            }
        }
    </script>
    @endif
@endsection