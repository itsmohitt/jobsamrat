
@extends('main')
@section('headContent')
    <title>Forum Home | Job Samrat</title>
    <link href="{!! URL::to('templates/conto/css/slidenav.almost-flat.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('slidenav.min.html') !!}" rel="stylesheet" type="text/css" />
@endsection
@section('pageName')
    <h2>Forum {!! (isset($head)?$head:'') !!}</h2>
@endsection
@section('script')

@endsection

@section('bodyContent')
    <section id="sp-top-a">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                @include('forum/forumHeader')
            </div>
            <div class="col-sm-9">
                <div class="panel panel-default">
                        <div class="uk-panel-body">
                        <table class=" table">
                            <tbody>
                            <?php $i=0; ?>
                            @foreach($topics as $topic)
                            <tr class="topicTable" name="{!! $topic->TopicId.'/'.str_replace(' ','-',$topic->TopicName) !!}">
                                <td>
                                    <div class="media">
                                        <a href="#" class="pull-left">
                                            <img alt="" src="{!! URL::to('images/users/user.jpg') !!}"  class="media-object img-circle img-responsive" style="min-width:50px;max-width:50px">
                                        </a>
                                        <div class="media-body">
                                            <span class="media-meta pull-right">{!! date('H:i A d-M-Y ',strtotime($topic->updated_at)) !!}</span>
                                            <h4 class="">{!!  $topic->TopicName !!}</h4>
                                            <small class="">User Name >>>> <a href="{!! URL::to('forum/overview/'.$topic->TopicCat.'/'.str_replace(' ','-',$topic->CatName)) !!}">{!! $topic->CatName !!}</a></small></br>
                                            <p class="email-summary">{!! $topic->TopicDescription !!}</p>

                                        </div>
                                    </div>
                                </td>

                            </tr>

                                <?php $i++; ?>
                            @endforeach
                            @if($i==0)
                                <tr >

                                    <td>
                                        <div class="media">

                                            <div class="media-body">
                                                <img alt="" src="{!! URL::to('images/nothingFound.png') !!}"  class="media-object pull-left img-responsive" style="max-width:200px;padding:10px">
                                                <h2 class="text-capitalize"> Nothing Was Found !!</h2>

                                                <p>Seems like this forum section  is empty yet, why don't you
                                                    @if(Auth::guest())
                                                        <a href="#" data-toggle="modal" data-target="#loginModal">Ask Question <i class="fa fa-question"></i></a>
                                                    @else
                                                    <a href="#" data-toggle="modal" data-target="#askQuestion">Ask Question <i class="fa fa-question"></i></a>
                                                    @endif
                                                </p>
                                                </br>

                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endif

                            </tbody>

                        </table>

                    <div  align="center">
                    {!! $topics->links() !!}
                    </div>
                            </div >
                </div>
            </div><!-- col-md-6 -->
            <div class="col-sm-3">

                @include('forum/forumRightPanel')
            </div>

            <div class="col-sm-9">

            </div>
        </div>
    </div><!-- contentpanel -->
    </section>
    <script>
        jQuery('.topicTable').click(function(){
            var topic_id=jQuery(this).attr('name');
            location.href="{!! URL::to('forum/') !!}/"+topic_id;

        } );
    </script>
@endsection