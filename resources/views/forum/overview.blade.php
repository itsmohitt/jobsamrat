@extends('main')
@section('headContent')
    <title>Forum Overview | Job Samrat </title>
@endsection
@section('pageName')
    <h2>Forum {!! (isset($head)?$head:'') !!}</h2>
@endsection
@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                @include('forum.forumHeader')
                </div>
                <div class="col-md-9">

                    <div class="sp-module title6">


                            <table class="table ">
                                <tbody>
                                <form action="#" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            <input type="text" name="keyword" placeholder="Search here..."  value="{!! (isset($keyword)?$keyword:'') !!}" class="form-control">
                                        </div>
                                        <div class="col-sm-3">
                                            {!! csrf_field() !!}
                                            <input type="submit" class="uk-button uk-button-primary btn btn-block"
                                                   value="Search" name="submit">
                                        </div>
                                    </div>
                                </form>
                                <br><br>
                                <?php  $i=0 ?>
                                @foreach($categories as $category)
                                    <tr class="topicTable" name="{!! $category->CatId !!}">

                                        <td>
                                            <div class="media">

                                                <div class="media-body">
                                                    <span class="media-meta pull-right"></span>
                                                    <h4 class="text-capitalize">{!! $category->CatName !!}</h4>


                                                    </br>

                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php $i++ ?>
                                @endforeach
                                @if($i==0)
                                    <tr >

                                        <td>
                                            <div class="media">

                                                <div class="media-body">
                                                    <img alt="" src="{!! URL::to('images/nothingFound.png') !!}"  class="media-object pull-left img-responsive" style="max-width:100px;padding:10px">
                                                    <h4 class="text-capitalize"> Nothing Was Found !!</h4>

                                                    <small >&nbsp;&nbsp;Followers</small>
                                                    </br>

                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                @endif
                                </tbody>
                            </table>

                    </div>
                </div><!-- col-md-6 -->
                <div class="col-md-3">
                    @include('forum.forumRightPanel')
                </div>
            </div>
        </div>
    </section>
    <script>
        jQuery('.topicTable').click(function(){
            var topic_id=jQuery(this).attr('name');
            location.href="{!! URL::to('forum/overview') !!}/"+topic_id;

        } );
    </script>
@endsection