<?php
/**
 * Created by PhpStorm.
 * User: mohit
 * Date: 25/7/16
 * Time: 4:54 PM
 */?>
<div class="sp-module title4" data-uk-scrollspy="{cls:'uk-animation-slide-right', repeat: false}">

        <h4>Categories</h4>
        <table class="table">
            <tbody>
          <?php /*  <tr><td><a href="#" class="uk-button uk-button-link btn btn-block">+ Add a category</a></td></tr> */ ?>
                @foreach($categories as $category)
                <tr><td><i class="fa fa-cog"></i> <a href="{!! URL::to('forum/overview/'.$category->CatId.'/'.str_replace(' ','-',$category->CatName)) !!}"> {!! $category->CatName !!}</a></td></tr>
                @endforeach
            </tbody>
        </table>

</div>
<div id="askQuestion" class="uk-modal" data-backdrop="static">
    <div class="uk-modal-dialog uk-modal-dialog-large"><a class="uk-modal-close uk-close"></a>
        <h4 class="modal-title">Ask Question in Forum</h4>
        <hr>
        <!-- Modal content-->

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/forum/add') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="category" class="col-md-4">Question Category</label>

                        <div class="col-md-8">
                            <select name="category" class="form-control" required autofocus>
                                @foreach($categories as $category)
                                    <option value="{!! $category->CatId !!}">{!! $category->CatName !!}</option>
                                @endforeach
                            </select>


                        </div>
                    </div>

                    <div class="form-group">
                        <label for="questionHead" class="col-md-4" >Question Heading</label>

                        <div class="col-md-8">
                            <textarea  class="form-control" name="questionHead" rows="4" required></textarea>


                        </div>
                    </div>
                    <div class="form-group">
                        <label for="questionHead" class="col-md-4" >Question Description</label>

                        <div class="col-md-12">
                            <textarea  class="form-control" name="questionBody" rows="3" max-length="500" required></textarea>


                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block">
                                Submit My Question
                            </button>


                        </div>
                    </div>
                </form>



</div>
</div>
@section('scriptContent')
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script>CKEDITOR.replace( 'questionBody' );</script>
@endsection