<?php
/**
 * Created by PhpStorm.
 * User: mohit
 * Date: 25/7/16
 * Time: 4:48 PM
 */?>
<div class="panel">


        <div class="btn-group btn-group-justified btn-group-lg">

                <a type="button" href="{!! URL::to('/forum') !!}" class="uk-button btn {!! (isset($head) && $head=='HOME'?'uk-button-primary':'') !!} " ><i class="fa fa-home"></i> Forum Home</a>

                <a type="button" href="{!! URL::to('/forum/search') !!}" class="uk-button btn {!! (isset($head) && $head=='SEARCH'?'uk-button-primary':'') !!} "><i class="fa fa-search"></i> <span>Search Topic</span></a>

                <a type="button" href="{!! URL::to('/forum/overview') !!}" class="uk-button btn {!! (isset($head) && $head=='OVERVIEW'?'uk-button-primary':'') !!} "><i class="fa fa-list"></i> <span>Browse Forum</span></a>
                @if(Auth::guest())
                        <a type="button" href="#" class="uk-button uk-button-success btn" data-toggle="modal" data-target="#loginModal"><i class="fa fa-question"></i> Ask Question</a>

                @else
                <a type="button" href="#askQuestion" data-uk-modal="{center:true}" class="uk-button uk-button-success btn" ><i class="fa fa-question"></i> Ask Question</a>
                @endif

        </div>



</div><!-- panel -->
