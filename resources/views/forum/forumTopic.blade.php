<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 2/2/2017
 * Time: 11:09 AM
 */ ?>
@extends('main')
@section('headContent')
    <title>
        {!! $topic->TopicName !!} | {!! $topic->CatName !!} | Job Samrat
    </title>
    <meta name=”description” content=”{!! $topic->TopicName !!}”/>
    <meta property=”og:title” content=”{!! $topic->TopicName !!}”/>
    <meta property=”og:type” content=”article”/>
    <meta property=”og:url” content=”http://www.jobsamrat.com/forum/{!! $topic->TopicId !!}/{!! str_replace(' ','-',$topic->TopicName) !!}}”/>
    <script src="../../../templates/conto/js/tooltip.min.js" type="text/javascript"></script>
@endsection
@section('bodyContent')
    <section id="sp-main-body">
        <div class="container">
            <div class="row">
                <div id="sp-component" class="col-sm-9 col-md-9">
                    <div class="sp-column ">
                        <div id="system-message-container">
                        </div>
                        <article class="item item-page " itemscope itemtype="http://schema.org/Article">
                            <meta itemprop="inLanguage" content="en-GB"/>
                           <?php /* <div class="entry-image full-image"><img src="../../../images/Demo/blog/blog4.jpg" alt="{!! $topic->TopicName !!}" itemprop="image"/></div> */ ?>
                            <div class="entry-header">

                                <dl class="article-info">
                                    <dt class="article-info-term"></dt>
                                    <dd class="published">
                                        <i class="fa fa-calendar-o"></i>
                                        <time datetime="{!! $topic->updated_at !!}" itemprop="datePublished" data-toggle="tooltip" title="Published Date">
                                            {!! date('d M Y', strtotime($topic->updated_at)) !!}
                                        </time>
                                    </dd>
                                    <dd class="category-name">
                                        <i class="fa fa-folder-open-o"></i>
                                        <a href="{!! URL::to('/forum/overview/'.$topic->TopicCat) !!}" itemprop="genre" data-toggle="tooltip" title="Category">{!! $topic->CatName !!}</a>
                                    </dd>

                                    <dd class="article-info">
                                        <i class="fa fa-user"></i>
                                        <a href="#" itemprop="genre" data-toggle="tooltip" title="Published By">{!! $topic->name !!}</a>
                                    </dd>
                                <?php  /*  <dd class="post_rating" id="post_vote_5">
                                        Rating:
                                        <div class="voting-symbol sp-rating">
                                            <span class="star active" data-number="5"></span>
                                            <span class="star active" data-number="4"></span>
                                            <span class="star active" data-number="3"></span>
                                            <span class="star active" data-number="2"></span>
                                            <span class="star active" data-number="1"></span>
                                        </div>
                                        <span class="ajax-loader fa fa-spinner fa-spin"></span>
                                        <span class="voting-result">( 1 Rating )</span>
                                    </dd> */ ?>

                                </dl>  <div class="icons">

                                    <div class="btn-group pull-right">
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span
                                                    class="icon-cog"></span><span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li class="print-icon">
                                                <a href="single-articleabf8.html?tmpl=component&amp;print=1&amp;page=" title="Print article < Accounting degrees overrated? >"
                                                        onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;"
                                                        rel="nofollow">
                                                    <span class="icon-print"></span>Print</a>
                                            </li>
                                            <li class="email-icon">
                                                <a href="../../component/mailto/indexaeb2.html?tmpl=component&amp;template=conto&amp;link=a838bdb9e317ec184a3bc48c97ed1c5bf476bee1" title="Email this link to a friend" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow">
                                                    <span class="icon-envelope"></span>Email</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <h2 itemprop="name">
                                    {!! $topic->TopicName !!} </h2>
                            </div>




                            <div class="content_rating" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
                                <p class="unseen element-invisible">User Rating:&#160;
                                    <span itemprop="ratingValue">5</span>
                                    &#160;/&#160;<span itemprop="bestRating">5</span>
                                    <meta itemprop="ratingCount" content="1"/>
                                    <meta itemprop="worstRating" content="0"/>
                                </p>
                                <img src="../../../media/system/images/rating_star.png" alt="Star Active"/>
                                <img src="../../../media/system/images/rating_star.png" alt="Star Active"/>
                                <img src="../../../media/system/images/rating_star.png" alt="Star Active"/>
                                <img src="../../../media/system/images/rating_star.png" alt="Star Active"/>
                                <img src="../../../media/system/images/rating_star.png" alt="Star Active"/></div>
                            <form method="post" action="http://dhtheme.com/conto/index.php/features/joomla/single-article?hitcount=0" class="form-inline">
                                <span class="content_vote">
                                    <label class="unseen element-invisible" for="content_vote_5">Please Rate</label>
                                    <select id="content_vote_5" name="user_rating">
                                        <option value="1">Vote 1</option>
                                        <option value="2">Vote 2</option>
                                        <option value="3">Vote 3</option>
                                        <option value="4">Vote 4</option>
                                        <option value="5" selected="selected">Vote 5</option>
                                    </select>
                                    &#160;<input class="btn btn-mini" type="submit" name="submit_vote" value="Rate"/>
                                    <input type="hidden" name="task" value="article.vote"/>
                                    <input type="hidden" name="hitcount" value="0"/>
                                    <input type="hidden" name="url" value="single-articlef463.html?hitcount=0"/>
                                    <input type="hidden" name="126bcf8cee83d50f075a0e0d09b9bb32" value="1"/>
                                </span>
                            </form>


                            <div itemprop="articleBody">
                                {!! $topic->TopicDescription !!}
                                </div>


                            <div class="tags">
                                <span>Tags: </span>
                                <span class="label label-info" rel="tag">{!! $topic->CatName !!}</span>


                            </div>
                            <hr>
                            <div class="row" id="comments">
                                <!-- Comments Form -->
                                <div class="sp-module title6">
                                    <h4>Leave a Comment:</h4>
                                    <form role="form" action="{!! URL::to('job/'.$topic->TopicId.'/comment') !!}" method="post">
                                        <div class="form-group">
                                            <textarea class="form-control" name="comment" rows="3"></textarea>
                                        </div>
                                        <input type="hidden" name="type" value="F">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>

                                <hr>

                            @foreach($comments as $comment)
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <img class="media-object img-circle" src="{!! URL::to('images/users/user.jpg') !!}"  width="50px" alt="">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">{!! $comment->name !!}
                                            <small>{!! date('d M Y, h:i A',strtotime($comment->updated_at)) !!}</small>
                                        </h4>
                                        {!! $comment->Comment !!}
                                    </div>
                                </div>
                            @endforeach


                        </article>
                    </div>
                </div>
                <div id="sp-right" class="col-sm-3 col-md-3">
                    <div class="sp-column class2">
                        <script>
                            (function() {
                                var cx = '000664330352881482014:irrwudbxgf0';
                                var gcse = document.createElement('script');
                                gcse.type = 'text/javascript';
                                gcse.async = true;
                                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(gcse, s);
                            })();
                        </script>
                        <gcse:search></gcse:search>
                        <div class="sp-module title6">
                            <h4 class="sp-module-title">You May Like</h4>
                            <div class="sp-module-content">
                                <ul class="list-unstyled ">
                                    @foreach($related as $relTopic)
                                        <li ><a href="{!! URL::to('forum/'.$relTopic->TopicId.'/'.str_replace(' ','-',$relTopic->TopicName)) !!}">{!! $relTopic->TopicName !!}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @include('forum.forumRightPanel')

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
