<header id="sp-header" class="container">
    <div class="container">
        <div class="row">
            <div id="sp-logo" class="col-xs-6 col-sm-3 col-md-3">
                <div class="sp-column ">
                    <a class="logo" href="{!! URL::to('/') !!}">
                        <h1>
                            <img class="sp-default-logo" src="{!! URL::to('images/jobSamratLogo.png') !!}" alt="Job Samrat">
                            <img class="sp-retina-logo" src="{!! URL::to('images/jobSamratLogo.png') !!}" alt="Job Samrat" width="172" height="45">
                        </h1>
                    </a>
                </div>
            </div>
            <div id="sp-menu" class="col-xs-4 col-sm-7 col-md-8">
                <div class="sp-column ">
                    <div class='sp-megamenu-wrapper'>
                        <a id="offcanvas-toggler" class="visible-xs visible-sm" href="#"><i class="fa fa-bars"></i></a>
                        <ul class="sp-megamenu-parent menu-fade hidden-xs hidden-sm">
                            <li class="sp-menu-item"><a  href="{!! URL::to('/') !!}" >Home</a></li>
                            <li class="sp-menu-item"><a  href="{!! URL::to('/forum') !!}" >Forum</a></li>
                            <li class="sp-menu-ite sp-has-child">
                                <a  href="{!! URL::to('job') !!}" >Notification</a>

                                <div class="sp-dropdown sp-dropdown-main sp-dropdown-mega sp-menu-full container" style="">
                                    <div class="sp-dropdown-inner">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul class="sp-mega-group">
                                                    <li class="sp-menu-item sp-has-child">
                                                        <a class="sp-group-title"  href="{!! URL::to('/job') !!}" >Job Notifications</a>
                                                        <ul class="sp-mega-group-child sp-dropdown-items">
                                                            <li class="sp-menu-item"><a  href="{!! URL::to('/job/latest') !!}" >Latest Jobs</a></li>
                                                            <li class="sp-menu-item"><a  href="{!! URL::to('/job/last') !!}" >Last Date Jobs</a></li>
                                                            <li class="sp-menu-item"><a  href="{!! URL::to('/job') !!}" >All Job Notification</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </li>

                            @if (Auth::guest())

                                <li><a href="{{ url('/register') }}">Register</a></li>
                            @else
                                <li class="sp-menu-item sp-has-child">
                                    <a href="#" >
                                        {{ Auth::user()->name }}
                                    </a>
                                    <div class="sp-dropdown sp-dropdown-main sp-dropdown-mega sp-menu-full " style="left:inherit">
                                        <div class="sp-dropdown-inner">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <ul class="sp-mega-group">
                                                        <li class="sp-menu-item sp-has-child">
                                                            <ul class="sp-mega-group-child sp-dropdown-items">
                                                                @if(Auth::user()->userType=='ADMIN')
                                                                    <li class="sp-menu-item"><a href="{!! URL::to('/admin/settings') !!}">Settings</a></li>
                                                                @else
                                                                    <li class="sp-menu-item"><a href="{!! URL::to('/profile') !!}">Profile</a></li>
                                                                    <li class="sp-menu-item"><a href="{!! URL::to('/settings') !!}">Settings</a></li>
                                                                @endif
                                                                <li class="sp-menu-item">
                                                                    <a href="{{ url('/logout') }}"
                                                                       onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                                                        Logout
                                                                    </a>

                                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                                        {{ csrf_field() }}
                                                                    </form>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </li>
                            @endif
                           </ul>
                    </div>

                </div>
            </div>

            <div id="sp-search" class="col-xs-2 col-sm-2 col-md-1">




                            @if(Auth::guest())
                                @if(!isset($setError) || $setError==false)
                                     <a class="uk-text-white" data-toggle="modal" data-target="#loginModal">Login</a>
                                @else
                                     <a class="uk-text-white" href="{!! URL::to('/login') !!}">Login</a>
                                @endif
                            @else
                                <a href="{!! URL::to('/profile') !!}"><img style="display:inline-block" class="img  img-circle" src="{!! URL::to('images/users/'.Auth::user()->userImg) !!}" alt="{!! Auth::user()->name !!}"></a>
                            @endif


            </div>
        </div>
    </div>
</header>
@if(!isset($setError) || $setError==false)
<div id="loginModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login Into Job Samrat</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">

                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endif
<section id="sp-page-title">
    <div class="container">
        <div class="row">
            <div id="sp-title" class="col-sm-12 col-md-12">
                <div class="sp-column "><div class="sp-module "><div class="sp-module-content">

                        </div>
                    </div>
                    <div class="sp-page-title">
                        <div class="container">
                            @yield('pageName')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
