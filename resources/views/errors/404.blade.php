<?php
$setError=true; ?>
@extends('main')
@section('headContent')

@endsection
@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2"align="center">
                    <img class="img-responsive uk-img-preserve" src="{!! URL::to('images/404.jpg') !!}" >
                    <div class="text-center">
                        <h2 style="margin:0px"><b>Now, that's embarassing.</b></h2>
                        <p>The page you are looking for has been removed or didn't exist.</p>
                        <a href="{!! URL::to('/') !!}" class="btn btn-primary btn-lg">Go to Home Page</a>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

