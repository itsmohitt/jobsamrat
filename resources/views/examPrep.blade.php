<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/13/2016
 * Time: 11:56 PM
 */?>
@extends('main')
@section('pageName')
    <h2>Exam Preparation</h2>
@endsection

@section('bodyContent')
    <section id="sp-top-a">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Page Content -->
<div class="container">

    <!-- Intro Content -->
    <div class="row">
        <div class="col-md-3">
            <div class="well">
                <h4>Exam Topics</h4>
                <ul class="list-unstyled">
                        @foreach($topics as $topic)
                        <li><a href="{!! URL::to('topic/'.$topic->TopicId.'/'.str_replace(' ','-',$topic->TopicName)) !!}">{!! $topic->TopicName !!}</a></li>
                        @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-3 pull-right">
            <script>
                (function() {
                    var cx = '000664330352881482014:irrwudbxgf0';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                })();
            </script>
            <gcse:search></gcse:search>
            <br>
            <div data-WRID="WRID-147844527591248304" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
            <br>
            <div class="fb-page" data-href="https://www.facebook.com/JobSamrat-1414555515239625/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/JobSamrat-1414555515239625/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/JobSamrat-1414555515239625/">Job Samrat</a></blockquote></div>
        </div>
    </div>
    <hr>
</div>
        </section>
@stop