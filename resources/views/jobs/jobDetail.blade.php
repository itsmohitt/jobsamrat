<?php
//var_dump($var);
$job=$var[0];
$simJobs=$var[1];
$comments=$var[2];
?>
@extends('main')
@section('headContent')
    <title>{!! $job->JobTitle !!}
        <?php $tags=explode(',',$job->JobTags) ?>
        @foreach($tags as $tag)
            {!!  ' | '.$tag !!}
        @endforeach
        </title>
@endsection
@section('pageName')
    <h2>{!! $job->JobTitle !!}</h2>
@endsection

@section('bodyContent')

    <section id="sp-top-a">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Page Content -->
    <div class="container">

        <!-- Intro Content -->
        <div class="row">
            <div class="col-md-9">
                <p><i class="fa fa-clock-o"></i> Last Updated On : <?php echo date('d-M-Y',strtotime($job->JobPost)); ?></p>
                <hr>
                <p class="lead">{!! $job->JobShortDescr !!}</p>
                <div >
                    {!! $job->JobDescr !!}
                </div>
                <hr>
                <div class="row" id="comments">
                    <!-- Comments Form -->
                    <div class="well">
                        <h4>Leave a Comment:</h4>
                        <form role="form" action="{!! URL::to('job/'.$job->JobId.'/comment') !!}" method="post">
                            <div class="form-group">
                                <textarea class="form-control" name="comment" rows="3"></textarea>
                            </div>
                            <input type="hidden" name="type" value="J">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>

                    <hr>

                    <!-- Posted Comments -->

                    <!-- Comment -->
                    @foreach($comments as $comment)
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object img-circle" src="{!! URL::to('images/users/user.jpg') !!}"  width="50px" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">{!! $comment->name !!}
                                    <small>{!! date('d M Y, h:i A',strtotime($comment->updated_at)) !!}</small>
                                </h4>
                                {!! $comment->Comment !!}
                            </div>
                        </div>
                        @endforeach

                                <!-- Comment -->

                </div>
            </div>
            <div class="col-md-3 pull-right">
                <script>
                    (function() {
                        var cx = '000664330352881482014:irrwudbxgf0';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>
                <gcse:search></gcse:search>
                <br>
                <div class="panel panel-success">
                    <div class="panel-body">
                        <h3>Similar Jobs</h3>
                        @foreach($simJobs as $simJob)
                            <p><a  href="{!! URL::to('job/'.$simJob->JobId.'/'.str_replace(' ','-',$simJob->JobShortName)) !!}" alt="{!! $simJob->JobShortName !!}">{!! $simJob->JobShortName !!}</a></p>

                        @endforeach

                    </div>
                </div>
                <div data-WRID="WRID-147844527591248304" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
                <br>
                <div class="fb-page" data-href="https://www.facebook.com/JobSamrat-1414555515239625/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/JobSamrat-1414555515239625/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/JobSamrat-1414555515239625/">Job Samrat</a></blockquote></div>
            </div>
        </div>

    </div>
    </section>
@stop