<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/13/2016
 * Time: 11:56 PM
 */?>
@extends('main')
@section('headContent')
    <title>Job Notifications
        @foreach($jobs as $job)
            <?php $tags=explode(',',$job->JobTags) ?>
            @foreach($tags as $tag)
                {!! ' | '.$tag !!}
            @endforeach
        @endforeach
    </title>
@endsection
@section('pageName')
    <h2>Job Notifications</h2>
@endsection

@section('bodyContent')
    <section id="sp-top-a">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Page Content -->
    <div class="container">

        <!-- Intro Content -->
        <div class="row">
            <div class="col-md-9">

                <div class="panel panel-success">
                    <div class="panel-body">
                        <a href="{!! URL::to('admin/settings/jobs/create') !!}"  class="btn btn-primary btn-block">Add New Job</a>
                        <hr>
                        <ul class="list-unstyled">
                        @foreach($jobs as $job)
                            <li>
                           <h4> <a href="{!! URL::to('job/'.$job->JobId.'/'.str_replace(' ','-',$job->JobShortName)) !!}">{!! $job->JobTitle !!}</a></h4>
                                <p>Last Date: {!! date('d-M-Y',strtotime($job->JobLastDate)) !!}&nbsp;
                                    @if(!Auth::guest() && Auth::user()->userType=='ADMIN')
                                        <a href="{!! URL::to('/admin/settings/jobs/edit/'.$job->JobId.'') !!}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Edit </a>&nbsp;
                                        <a href="{!! URL::to('/admin/settings/jobs/'.$job->JobId.'/delete') !!}" onclick="confirm(`Are you confirm to delete  '{!! $job->JobTitle !!}'`)" class="btn btn-sm btn-danger"><i class="fa fa-cross"></i> Delete</a></p>
                                    @endif
                            <p><small>{!! $job->JobShortDescr !!}</small></p>
                            </li>
                            <hr>
                        @endforeach
                        </ul>
                        {!! $jobs->links() !!}

                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-right">
                <script>
                    (function() {
                        var cx = '000664330352881482014:irrwudbxgf0';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>
                <gcse:search></gcse:search>
                <br>
                <div data-WRID="WRID-147844527591248304" data-widgetType="Push Content"  data-class="affiliateAdsByFlipkart" height="250" width="300"></div><script async src="//affiliate.flipkart.com/affiliate/widgets/FKAffiliateWidgets.js"></script>
                <br>
                <div class="fb-page" data-href="https://www.facebook.com/JobSamrat-1414555515239625/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/JobSamrat-1414555515239625/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/JobSamrat-1414555515239625/">Job Samrat</a></blockquote></div>
            </div>
        </div>
        <hr>
    </div>
    </section>

@stop