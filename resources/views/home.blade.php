<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/13/2016
 * Time: 10:04 PM
 */?>
@extends('main')
@section('headContent')
    <title>Job Samrat | Home</title>
    <script src="{!! URL::to('media/system/js/mootools-more.js') !!}" type="text/javascript"></script>
@stop
@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <div class="sp-module title6">
                        <h3 class="sp-module-title">Latest Jobs</h3>
                        <?php

                        $latest=$var[0];
                        $important=$var[1];
                        $topics=$var[2];
                        ?>
                        @foreach($latest as $latNoti)
                            <p>
                                <a href="{!! URL::to('job/'.$latNoti->JobId.'/'.str_replace(' ','-',$latNoti->JobShortName)) !!}">
                                    {!! $latNoti->JobTitle !!}
                                </a>
                                <br>
                                <small>Last Date: {!! date('d M Y',strtotime($latNoti->JobLastDate)) !!}</small>
                            </p>
                        @endforeach
                        <a class="uk-button uk-button-primary btn-block uk-margin-top" href="{!! URL::to('job') !!}">More Latest Jobs</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sp-module title6">
                        <h3 class="sp-module-title">Important</h3>
                        @foreach($important as $impNoti)
                            <p>
                                <a href="{!! URL::to('job/'.$impNoti->JobId.'/'.str_replace(' ','-',$impNoti->JobShortName)) !!}">
                                    {!! $impNoti->JobTitle !!}
                                </a>
                                <br>
                                <small>Last Date: {!! date('d M Y',strtotime($impNoti->JobLastDate)) !!}</small>
                            </p>
                        @endforeach
                        <a class="uk-button uk-button-primary btn-block uk-margin-top">More Important Notice</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sp-module title6">
                        <h3 class="sp-module-title">Exam Preparation</h3>
                        <article class="item item-page" itemscope itemtype="http://schema.org/Article">
                            <meta itemprop="inLanguage" content="en-GB" />



                            <div class="entry-header">


                            </div>





                            <div itemprop="articleBody">
                                <div class="sprocket-lists" data-lists="108">
                                    <ul class="sprocket-lists-container" data-lists-items>
                                        @foreach($topics as $topic)
                                            <li class="active" data-lists-item>
                                                <h5 class="sprocket-lists-title padding" data-lists-toggler>
                                                    {!! $topic[0]->TopicName !!}			<span class="indicator"></span>	</h5>
                                                        <span class="sprocket-lists-item" data-lists-content>
                                                        <span class="sprocket-padding">
                                                        <ul class="list-unstyled">
                                                            @foreach($topic[1] as $subTopic)
                                                                <li><a href="{!! URL::to('subTopic/'.$subTopic->SubTopicId.'/'.str_replace(' ','-',$subTopic->SubTopicName)) !!}">{!! $subTopic->SubTopicName !!}</a></li>
                                                            @endforeach
                                                            <a class="uk-button uk-button-primary  btn btn-sm uk-margin-top" href="{!! URL::to('examPrep') !!}">More </a>
                                                        </ul>
                                                     </span>
                                                    </span>

                                            </li>
                                        @endforeach




                                    </ul>
                                    <a class="uk-button uk-button-primary btn-block uk-margin-top" href="{!! URL::to('examPrep') !!}">More Exam Topics</a>
                                    <div class="sprocket-lists-nav">
                                        <div class="sprocket-lists-pagination-hidden">
                                            <ul>
                                                <li class="active" data-lists-page="1"><span>1</span></li>
                                            </ul>
                                        </div>
                                        <div class="spinner"></div>
                                    </div>
                                </div>
                            </div>







                        </article>

                    </div>
                </div>


            </div>

        </div>
    </section>
    <!--
    <section id="sp-top-b"><div class="container"><div class="row"><div id="sp-top7" class="col-sm-12 col-md-12"><div class="sp-column "><div class="sp-module title1"><h3 class="sp-module-title">Industries</h3><div class="sp-module-content">

                                <div class="customtitle1"  >
                                    <div data-uk-grid-margin="" class="uk-grid uk-grid-match uk-grid-width-1-1 uk-grid-width-medium-1-2 uk-grid-width-large-1-3">
                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry1.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Financial Services</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>

                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry2.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel">
                                                        <h4>Public Sector</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-inverted uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry3.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Technology</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry4.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel">
                                                        <h4>Construction</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-inverted uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>

                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry5.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Healthcare</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div></div>
                            </div></div></div></div></div></div></section> -->
                            @include('advertiseFooter')
    @endsection
