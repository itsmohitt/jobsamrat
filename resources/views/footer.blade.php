<section id="sp-bottom-b">
    <div class="container">
        <div class="row">
            <div id="sp-bottom9" class="col-sm-3 col-md-4">
                <div class="sp-column ">
                    <div class="sp-module ">
                        <h3 class="sp-module-title">About</h3>
                        <div class="sp-module-content">
                            <div class="custom"  >
                                <p>Job Samrat is the best job finding and preparation site by providing you the new and interesting way to do it.
                                    <br>
                                    And latest news so that you are updated with the changing environment.</p>
                                <br>
                                <p><img alt="About Conto" src="{!! URL::to('images/jobSamratLogo.png') !!}"></p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sp-bottom10" class="col-sm-3 col-md-2">
                <div class="sp-column ">
                    <div class="sp-module ">
                        <h3 class="sp-module-title">Sitemap</h3>
                        <div class="sp-module-content">
                            <p>

                                <a href="{!! URL::to('/') !!}"  ><i class="fa fa-long-arrow-right "></i> Home</a>

                            </p>
                            @unless(Auth::check())
                                <p>

                                    <a href="{!! URL::to('/login') !!}"  ><i class="fa fa-long-arrow-right "></i> Login</a>

                                </p>
                            @endunless
                            <p>

                                <a href="{!! URL::to('/forum') !!}"  ><i class="fa fa-long-arrow-right "></i> Forum</a>

                            </p>
                            <p>

                                <a href="{!! URL::to('examPrep') !!}"  ><i class="fa fa-long-arrow-right "></i> Exam Preparation</a>

                            </p>
                            <p>

                                <a href="{!! URL::to('about') !!}"  ><i class="fa fa-long-arrow-right "></i> About Us</a>

                            </p><p>

                                <a href="{!! URL::to('contact') !!}" ><i class="fa fa-long-arrow-right "></i> Contact Us</a>

                            </p><p>

                                <a href="{!! URL::to('privacyPolicy') !!}" ><i class="fa fa-long-arrow-right "></i> Privacy Policy</a>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sp-bottom11" class="col-sm-3 col-md-3">
                <div class="sp-column ">
                    <div class="sp-module ">
                        <h3 class="sp-module-title">Connect with us</h3>
                        <div class="sp-module-content">
                            <p><a href="http://facebook.com/JobSamrat-1414555515239625" target="_blank"><i class="fa fa-facebook fa-2x"></i> | Job Samrat</a></p>

                            <p><a href="#"><i class="fa fa-envelope fa-2x"></i> | info@jobsamrat.com </a> </p>

                            <p><a href="#"><i class="fa fa-envelope fa-2x"></i> | jobsamratofficial@gmail.com </a></p>

                            <p><a href="#"><i class="fa fa-map-marker fa-2x"></i> | New Delhi, INDIA </a>

                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <div id="sp-bottom12" class="col-sm-3 col-md-3">
                <div class="sp-column ">
                    <div class="sp-module ">
                        <h3 class="sp-module-title">Contact</h3>
                        <div class="sp-module-content">
                            <div class="custom"  >
                                <form method="post" id="subscribeForm">

                                    <p><small>1000+ subscribers can not be wrong. Subscribe and get daily update on mail...</small></p>

                                    <input class="form-control" type="text" id="subscribeId" placeholder="john@dailymail.com">

                                    <br>

                                    <div id="subscribeSuccess"></div>

                                    <input type="submit" class="btn btn-block btn-primary" id="subscribeSubmit" value="Subscribe!">

                                    <br>

                                </form>

                                <div id="google_translate_element"></div><script type="text/javascript">
                                    function googleTranslateElementInit() {
                                        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'bn,en,gu,hi,ml,mr,ne,pa,sd,ta,te,th,ur'}, 'google_translate_element');
                                    }
                                </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</section>
<footer id="sp-footer">
    <div class="container">
        <div class="row">
            <div id="sp-footer1" class="col-sm-12 col-md-12"><div class="sp-column "><div class="sp-module "><div class="sp-module-content">
                            <div class="custom">
                                <a href="#sp-page-title" class="to-top uk-icon-chevron-up" data-uk-smooth-scroll="{offset: 30}"></a></div>
                        </div>
                    </div>
                    <span > Copyright © Job Samrat <?php echo date('Y'); ?>. All Right Reserved. Yup,We Do.</span>&nbsp;&nbsp;&nbsp; <span>Designed By <a href="http://www.cuberootinfotech.com" target="_blank">Cube Root</a> With <i class="fa fa-heart" onclick="pulse()" style="color:#ff5f5f"></i> in India</span>
                </div>
            </div>
        </div>
    </div>
</footer>
