<section id="sp-bottom-a">
    <div class="container">
        <div class="row">
            <div id="sp-bottom5" class="col-sm-12 col-md-12">
                <div class="sp-column ">
                    <div class="sp-module ">
                        <div class="sp-module-content">
                            <div class="custom"  >
                                <div class="uk-grid">
                                    <div class="uk-width-large-3-4">
                                        <h4>Connect with more than 10,000 people with Job Samrat</h4>
                                        <p>With more than 1 million pageviews and 1000 unique visitors a month, Job Samrat offers advertisers integrated advertising and promotional programs to reach our highly affluent and educated audience . </p>
                                    </div>
                                    <div class="uk-width-large-1-4 uk-text-right">
                                        <a href="contact#" class="uk-button uk-button-large uk-margin-top">ADVERTISE WITH US</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>