<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msvalidate.01" content="D20295D2F136E1FD8E993306DD44EBE8" />
    <link rel=”author” href=”https://plus.google.com/101528014807436753612”/>
    <link rel=”publisher” href=”https://plus.google.com/101528014807436753612”/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Job Samrat" />
    <meta name="generator" content="Job Samrat | Find Your Job" />

    <link href="{!! URL::to('images/favicon.png') !!}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="{!! URL::to('index.php/component/search/index4189.html?id=1&amp;Itemid=101&amp;format=opensearch') !!}" rel="search" title="Search Conto - Accounting, Tax, Finance and Consulting Joomla Template" type="application/opensearchdescription+xml" />
    <link href="{!! URL::to('templates/conto/roksprocket/layouts/strips/themes/default/strips.css') !!}" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Montserrat:regular,700&amp;subset=latin" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/slidenav.almost-flat.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/tooltip.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/progress.almost-flat.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/uikit.almost-flat.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('slidenav.min.html') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/legacy.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/template.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/css/presets/preset1.css') !!}" rel="stylesheet" type="text/css" class="preset" />
    <link href="{!! URL::to('templates/conto/roksprocket/layouts/quotes/themes/default/quotes.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/roksprocket/layouts/strips/themes/separated/separated.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('templates/conto/roksprocket/layouts/lists/themes/default/lists.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('components/com_roksprocket/layouts/strips/themes/parallel/parallel.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('media/com_uniterevolution2/assets/rs-plugin/css/settings.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('media/com_uniterevolution2/assets/rs-plugin/css/dynamic-captions.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! URL::to('media/com_uniterevolution2/assets/rs-plugin/css/static-captions.css') !!}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        body{font-family:Montserrat, sans-serif; font-weight:normal; }
        h1{font-family:Montserrat, sans-serif; font-size:42px; font-weight:700; }
        h2{font-family:Montserrat, sans-serif; font-size:38px; font-weight:700; }
        h3{font-family:Montserrat, sans-serif; font-size:32px; font-weight:700; }
        h4{font-family:Montserrat, sans-serif; font-size:24px; font-weight:normal; }
        h5{font-family:Montserrat, sans-serif; font-size:16px; font-weight:normal; }
        h6{font-family:Montserrat, sans-serif; font-weight:normal; }
        .sp-megamenu-parent{font-family:Montserrat, sans-serif; font-size:14px; font-weight:700; }
        #sp-bottom-b{ background-image:url("images/Demo/elements/footer.jpg");background-repeat:no-repeat;background-size:cover;background-attachment:fixed;background-position:50% 0; }
    </style>
    <script src="{!! URL::to('media/system/js/mootools-core.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('media/system/js/core.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/assets/js/mootools-mobile.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/assets/js/rokmediaqueries.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/assets/js/roksprocket.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/assets/js/moofx.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/assets/js/roksprocket.request.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/strips/assets/js/strips.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/strips/assets/js/strips-speeds.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('media/jui/js/jquery.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('media/jui/js/jquery-noconflict.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('media/jui/js/jquery-migrate.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('media/system/js/caption.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/searchme.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/grid.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/grid.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/uikit.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/bootstrap.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/jquery.sticky.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/main.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/lightbox.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/parallax.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('templates/conto/js/tooltip.min.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/quotes/assets/js/quotes.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/quotes/assets/js/quotes-speeds.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/lists/assets/js/lists.js') !!}" type="text/javascript"></script>
    <script src="{!! URL::to('components/com_roksprocket/layouts/lists/themes/default/lists.js') !!}" type="text/javascript"></script>
    @yield('headContent')

    <script type="text/javascript">
        if (typeof RokSprocket == 'undefined') RokSprocket = {};
        Object.merge(RokSprocket, {
            SiteURL: 'http://dhtheme.com/conto/',
            CurrentURL: 'http://dhtheme.com/conto/',
            AjaxURL: 'http://dhtheme.com/conto/index.php?option=com_roksprocket&amp;task=ajax&amp;format=raw&amp;ItemId=101'
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.strips = new RokSprocket.Strips();
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.strips.attach(97, '{"animation":"fadeDelay","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-strips="97"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        jQuery(window).on('load',  function() {
            new JCaption('img.caption');
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.quotes = new RokSprocket.Quotes();
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.quotes.attach(99, '{"animation":"fadeDelay","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-quotes="99"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.strips.attach(98, '{"animation":"fadeDelay","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-strips="98"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.strips.attach(96, '{"animation":"fadeDelay","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-strips="96"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.lists = new RokSprocket.Lists();
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.lists.attach(94, '{"accordion":"1","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-lists="94"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.strips.attach(91, '{"animation":"fadeDelay","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-strips="91"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });

    </script>
    <script type="text/javascript">
        if (typeof RokSprocket == 'undefined') RokSprocket = {};
        Object.merge(RokSprocket, {
            SiteURL: 'http://dhtheme.com/conto/',
            CurrentURL: 'http://dhtheme.com/conto/',
            AjaxURL: 'http://dhtheme.com/conto/index.php?option=com_roksprocket&amp;task=ajax&amp;format=raw&amp;ItemId=114'
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.lists = new RokSprocket.Lists();
        });
        window.addEvent('domready', function(){
            RokSprocket.instances.lists.attach(108, '{"accordion":"1","autoplay":"0","delay":"5"}');
        });
        window.addEvent('load', function(){
            var overridden = false;
            if (!overridden && window.G5 && window.G5.offcanvas){
                var mod = document.getElement('[data-lists="108"]');
                mod.addEvents({
                    touchstart: function(){ window.G5.offcanvas.detach(); },
                    touchend: function(){ window.G5.offcanvas.attach(); }
                });
                overridden = true;
            };
        });
        jQuery(window).on('load',  function() {
            new JCaption('img.caption');
        });
        jQuery(function($){ $(".hasTooltip").tooltip({"html": true,"container": "body"}); });
    </script>
    <style>
       body  *::-webkit-scrollbar {
            width: 6px;
        }

       body  *::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.3);
        }

        body *::-webkit-scrollbar-thumb {
            background-color: darkgrey;
            outline: 1px solid slategrey;
        }
    </style>
</head>
<body class="site com-content view-article no-layout no-task itemid-101 en-gb ltr  sticky-header layout-fluid">
<div class="body-innerwrapper">
@include('header')
        <!-- Page Content -->
    @yield('bodyContent')
<!-- /.container -->

@include('footer')

    @include('rightHeader')
</div>
        <!-- jQuery -->
<script src="{!! URL::to('templates/conto/js/jquery.js') !!}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{!! URL::to('templates/conto/js/bootstrap.min.js') !!}"></script>
@yield('scriptContent')

</body>
</html>


