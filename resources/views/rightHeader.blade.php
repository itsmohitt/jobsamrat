<div class="offcanvas-menu">
    <a href="#" class="close-offcanvas"><i class="fa fa-remove"></i></a>
    <div class="offcanvas-inner">
        <div class="sp-module ">
            <div class="sp-module-content">
                <ul class="nav menu">
                    @if (Auth::guest())

                        <li class="item-106"><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="item-106 deeper parent"><a href="#" >{!! Auth::user()->name !!}</a>
                            <ul class="nav-child unstyled small">
                                @if(Auth::user()->userType=='ADMIN')
                                    <li class="sp-menu-item"><a href="{!! URL::to('/admin/settings') !!}">Settings</a></li>
                                @else
                                    <li class="sp-menu-item"><a href="{!! URL::to('/profile') !!}">Profile</a></li>
                                    <li class="sp-menu-item"><a href="{!! URL::to('/settings') !!}">Settings</a></li>
                                @endif
                                <li class="sp-menu-item">
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="item-101 default current active"><a  href="{!! URL::to('/') !!}" >Home</a></li>

                    <li class="item-106 deeper parent"><a href="#" > Notification</a>
                        <ul class="nav-child unstyled small">
                            <li class="item-109 deeper parent"><a class="sp-group-title"  href="{!! URL::to('/job') !!}" >Job Notifications</a>
                                <ul class="nav-child unstyled small">
                                    <li class="item-103"><a  href="{!! URL::to('/job/latest') !!}" >Latest Jobs</a></li>
                                    <li class="item-113"><a  href="{!! URL::to('/job/last') !!}" >Last Date Jobs</a></li>
                                    <li class="item-114"><a  href="{!! URL::to('/job') !!}" >All Job Notification</a></li>

                                </ul>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
