<?php
/**
 * Created by PhpStorm.
 * User: Billy
 * Date: 11/13/2016
 * Time: 10:04 PM
 */?>
@extends('main')
@section('headContent')
    <title>Job Samrat | Home</title>
@stop
@section('bodyContent')
    <section id="sp-top-a">
        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="{!! URL::to('images/jobSamrat.jpg') !!}" alt="">
                </div>
                <div class="col-md-6">
                    <h2>About Job Samrat</h2>
                    <p>We are here with new facility and techniques to find the latest pattern of syllabus and latest forms related to job & academic preparation .</p>
                    <p>Our vision is to be the best job finding and preparation site by providing you the new and interesting way to do it. We provide deep analysis of your performance through our intuitive graphical reports and help you improve your skills by pointing out your weak areas.And latest news so that you are updated with the changing environment.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Disclaimer</h2>
                    <p>The Examination Results / Marks published in this Website is only for the immediate Information to the Examinees and does not to be a constitute to be a Legal Document. While all efforts have been made to make the Information available on this Website as Authentic as possible. We are not responsible for any Inadvertent Error that may have crept in the Examination Results / Marks being published on this Website and for any loss to anybody or anything caused by any Shortcoming, Defect or Inaccuracy of the Information on this Website.The Personal information, email that submitted while registering to the site, will NOT be distributed, shared with any other third-parties. We only use this data for our information, for research, to improve our services and for contacting you purposes.Any significant changes will be notified to you by sending an email to your email address that you provided while registering with us or by placing a notice on our site.</p>
                </div>
            </div>
            <!-- /.row -->
            <hr>



        </div>

        </div>
    </section>
    <!--
    <section id="sp-top-b"><div class="container"><div class="row"><div id="sp-top7" class="col-sm-12 col-md-12"><div class="sp-column "><div class="sp-module title1"><h3 class="sp-module-title">Industries</h3><div class="sp-module-content">

                                <div class="customtitle1"  >
                                    <div data-uk-grid-margin="" class="uk-grid uk-grid-match uk-grid-width-1-1 uk-grid-width-medium-1-2 uk-grid-width-large-1-3">
                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry1.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Financial Services</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>

                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry2.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel">
                                                        <h4>Public Sector</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-inverted uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry3.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Technology</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry4.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel">
                                                        <h4>Construction</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-inverted uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>

                                            <div class="uk-panel">
                                                <figure class="uk-overlay">
                                                    <img class="uk-border-rounded" src="images/Demo/elements/industry5.jpg" alt="Industries">
                                                    <figcaption class="uk-overlay-panel uk-text-default">
                                                        <h4>Healthcare</h4>
                                                        <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit,<br> sed do eiusmod tempor.</p>
                                                        <a class="uk-button uk-button-primary uk-margin-top" href="#">READ MORE</a>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </div>
                                    </div></div>
                            </div></div></div></div></div></div></section>
    @endsection
