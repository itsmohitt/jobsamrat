<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table    = 'comment';
    protected $primaryKey= 'CommentId';
    protected $fillable =[
         'Comment',
        'CommentRefID',
        'CommentType',
        'updated_at',
        'created_at',
        'CommentStatus'
    ];
}
