<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use App\Http\Controllers\forum;
class ForumCategoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('forum.forumRightPanel',function($view){
            $category=DB::table('forumcat')->where('CatStatus','1')->get();
            $view->with('categories',$category);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
