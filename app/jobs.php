<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobs extends Model
{
    protected $table        = 'jobsnoti';
    protected $primaryKey   = 'JobId';
    protected $fillable     = [
     'JobTitle','JobShortName','JobShortDescr','JobDescr','JobLastDate','JobImp'
    ];
}
