<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forumTopic extends Model
{
    protected $table = 'forumtopic';
    protected $primaryKey = 'TopicId';
    protected $fillable = [
        'TopicName',
        'TopicDescription',
        'TopicCat',
        'TopicStatus',
        'TopicUser'
    ];
}
