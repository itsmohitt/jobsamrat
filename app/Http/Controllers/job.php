<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class job extends Controller
{
    function home(){
        $today=date('Y-m-d');
        $lessDays=date('Y-m-d',strtotime($today .'+ 15 days'));
        $latvalue=DB::table('jobsnoti')->where('JobStatus','1')->orderby('JobId','DESC')->limit(10)->get();

        $impvalue=DB::table('jobsnoti')->where(function($query) use ($lessDays,$today){
            $query->where([
                ['JobLastDate', '<=', $lessDays]
                , ['JobLastDate', '>=', $today]
            ])->orwhere([
                ['JobAdmitLastDate', '<=', $lessDays]
                , ['JobAdmitLastDate', '>=', $today]
            ]);
        })->where('JobStatus','1')->limit(10)->orderby('JobLastDate','ASC')->get();

        $topicRow=DB::table('quetopic')->where('TopicStatus','1')->limit(12)->get();
        $topics=array();
        foreach($topicRow as $topic){
            $subTopicRow=DB::table('quesubtopic')->where('TopicId',$topic->TopicId)->limit(10)->get();
            array_push($topics,array($topic,$subTopicRow));
        }
        return view('home')->with('var',array($latvalue,$impvalue,$topics));
    }

    function latest(){
        $today=date('Y-m-d');
        $jobs=DB::table('jobsnoti')->where('JobStatus','1')->where('JobLastDate', '>=', $today)->orderby('JobId','DESC')->paginate(8);
        return view('jobs.job')->with('jobs',$jobs);
    }

    function last(){
        $today=date('Y-m-d');
        $jobs=DB::table('jobsnoti')->where('JobStatus','1')->where('JobLastDate', '>=', $today)->orderby('JobLastDate','ASC')->paginate(8);
        //return $today;
       return view('jobs.job')->with('jobs',$jobs);
    }

    function jobDetail($id){
        $jobs=DB::table('jobsnoti')->where('JobId',$id)->first();
        $tags=explode(',',$jobs->JobTags);
        $firstTag="'%".implode("','%",$tags)."'";
        $lastTag="'".implode("%','",$tags)."%'";
        $middleTag="'%".implode("%','%",$tags)."%'";
        $selfTag="'".implode("','",$tags)."'";
        $simJobs=DB::table('jobsnoti')->where(function($query) use ($firstTag,$lastTag,$middleTag,$selfTag){
          $query->whereraw('JobTags IN ('.$firstTag.') OR JobTags IN ('.$lastTag.') OR JobTags IN ('.$middleTag.') OR JobTags IN ('.$selfTag.')');
        })->limit(5)->get();

        $comment = DB::table('comment')->leftjoin('users','users.id', '=', 'comment.CommentUser')->where('CommentType','J')->where('CommentRefID',$id)->where('CommentStatus','1')->orderby('comment.updated_at','DESC')->get();

        return view('jobs.jobDetail')->with('var',array($jobs,$simJobs,$comment));
    }
    function jobs(){
        $jobs=DB::table('jobsnoti')->where('JobStatus','1')->orderby('JobId','DESC')->paginate(8);

        return view('jobs.job')->with('jobs',$jobs);
    }


}
