<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\jobs;
use App\ques;
use App\quetopic;
use App\quesubtopic;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function adminHome(){
        return view('admin.index');
    }
    function createjob(){
        return view('admin.jobCreate');
    }

    function storeJob(Request $request){
        $job=new jobs;
        $job->JobTitle      = $request['jobTitle'];
        $job->JobShortName  = $request['jobShortTitle'];
        $job->JobShortDescr = $request['jobShortDescr'];
        $job->JobDescr      = $request['jobDescr'];
        $job->JobLastDate   = $request['jobLastDate'];
        $job->JobImp        = (isset($request['jobImp'])&& $request['jobImp']=='YES'?'1':'0');
        $job->JobTags       = $request['jobTags'];
        $job->save();
        return redirect('admin/settings/jobs/create');
    }

    function editJob($id){
        $job=DB::table('jobsnoti')->where('JobId',$id)->first();
        return view('admin.jobEdit')->with('job',$job);
    }
    function updateJob(Request $request,$id){
        $job=jobs::where('JobId',$id)->first();
        $job->JobTitle      = $request['jobTitle'];
        $job->JobShortName  = $request['jobShortTitle'];
        $job->JobShortDescr = $request['jobShortDescr'];
        $job->JobDescr      = $request['jobDescr'];
        $job->JobLastDate   = $request['jobLastDate'];
        $job->JobImp        = (isset($request['jobImp'])&& $request['jobImp']=='YES'?'1':'0');
        $job->JobTags       = $request['jobTags'];
        $job->save();
        return redirect('/job/'.$id.'/'.str_replace(' ','-',$job->JobShortName));
    }
    function questions(){
        $question=DB::table('jobque')->orderby('updated_at','DESC')->paginate(12);
        return view('admin.questions')->with('questions',$question);
    }
    function createQue(){
        $subCat=DB::table('quesubtopic')->where('SubTopicStatus','1')->get();
        return view('admin.queCreate')->with('subTopics',$subCat);
    }
    function storeQue(Request $request){
        $que = new ques;
        $que->Question = $request['queTitle'];
        $que->OptionA = $request['optionA'];
        $que->OptionB = $request['optionB'];
        $que->OptionC = $request['optionC'];
        $que->OptionD = $request['optionD'];
        $que->OptionE = $request['optionE'];
        $que->QueAnswer = $request['queAnswer'];
        $que->QueDifficulty = $request['queDifficulty'];
        $que->SubTopicId = implode(',',$request['subTopic']);
        $que->save();

        return redirect('admin/settings/questions');
    }
    function editQue($id){
        $que=DB::table('jobque')->where('QueId',$id)->first();
        $subCat=DB::table('quesubtopic')->where('SubTopicStatus','1')->get();
        return view('admin.queEdit')->with('subTopics',$subCat)->with('que',$que);
    }
    function updateQue(Request $request, $id){
        $que = ques::where('QueId',$id);
        $que->Question = $request['queTitle'];
        $que->OptionA = $request['optionA'];
        $que->OptionB = $request['optionB'];
        $que->OptionC = $request['optionC'];
        $que->OptionD = $request['optionD'];
        $que->OptionE = $request['optionE'];
        $que->QueAnswer = $request['queAnswer'];
        $que->QueDifficulty = $request['queDifficulty'];
        $que->SubTopicId = implode(',',$request['subTopic']);
        $que->save();
        return redirect('admin/settings/questions');
    }


    function category(){
        $topicRow=DB::table('quetopic')->where('TopicStatus','1')->get();
        $topics=array();
        foreach($topicRow as $topic){
            $subTopicRow=DB::table('quesubtopic')->where('TopicId',$topic->TopicId)->where('SubTopicStatus',1)->get();
            array_push($topics,array($topic,$subTopicRow));
        }
        return view('ques.category')->with('topics',$topics);
    }
    function categoryAdd(Request $request){
        $topic = new quetopic;
        $topic->TopicName = $request['category'];
        $topic->save();
        return redirect()->back();
    }
    function categoryUpdate(Request $request,$id){

        $topic  = quetopic::where('TopicId',$id)->first();
        $topic->TopicName = $request['category'];
        $topic->save();
        return redirect()->back();
    }
    function categoryDelete(Request $request){
        $id     = $request['topic'];
        $topic  = quetopic::where('TopicId',$id)->first();
        $topic->TopicStatus = 0;
        $topic->save();

    }
    function subCategoryAdd(Request $request){
        $topic = new quesubtopic;
        $topic->TopicId     = $request['category'];
        $topic->SubTopicName = $request['subCategory'];
        $topic->save();
        return redirect()->back();
    }
    function subCategoryUpdate(Request $request,$id){

        $topic  = quesubtopic::where('SubTopicId',$id)->first();
        $topic->TopicId     = $request['category'];
        $topic->SubTopicName = $request['subCategory'];
        $topic->save();
        return redirect()->back();
    }
    function subCategoryDelete(Request $request){
        $id     = $request['subTopic'];
        $topic  = quesubtopic::where('SubTopicId',$id)->first();
        $topic->SubTopicStatus = '0';
        $topic->save();
        return "SUCCESS";
    }
}
