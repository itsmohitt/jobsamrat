<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comment;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function profile()
    {
        return view('user.profile');
    }

    public function comment(Request $request,$id){
        $comment =new comment;
        $comment->Comment       = $request['comment'];
        $comment->CommentRefID  = $id;
        $comment->CommentType   = $request['type'];
        $comment->CommentUser   = Auth::user()->id;
        $comment->save();
        return redirect()->back();
    }
}
