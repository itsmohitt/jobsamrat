<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\forumTopic;
use Auth;
class forum extends Controller
{
   /*
   Code for Forum Right Panel Caterories are in ForumCategory Service Provider
   */
    function home(){
        $topic=DB::table('forumtopic')->leftjoin('forumcat','forumtopic.TopicCat','=','forumcat.CatId')->select('TopicId','TopicName','TopicDescription','forumtopic.updated_at','CatName','TopicCat')->orderby('forumtopic.created_at','DESC')->paginate(8);
        return view('forum.home')->with(['topics'=>$topic,'head'=>'HOME']);
    }

    function catOverview($id){
        $category=DB::table('forumcat')->where(['CatStatus'=>'1','CatId'=>$id])->first();
        if(empty($category)){
            return redirect('/forum');
        }
        $topic=DB::table('forumtopic')->where('TopicCat',$id)->leftjoin('forumcat','forumtopic.TopicCat','=','forumcat.CatId')->select('TopicId','TopicName','TopicDescription','forumtopic.updated_at','CatName','TopicCat')->paginate(8);
        $topicName=$category->CatName;
        return view('forum.home')->with(['topics'=>$topic,'head'=>'/ '.$topicName]);
    }
    function overview(){
        $category=DB::table('forumcat')->where('CatStatus','1')->get();
        return view('forum.overview')->with(['categories'=>$category,'head'=>'OVERVIEW']);
    }
    function searchOverview(Request $request){
        $search=$request['keyword'];
        $category=DB::table('forumcat')->where(['CatStatus'=>'1'])->where('CatName','LIKE' ,'%'.$search.'%')->get();
        return view('forum.overview')->with(['categories'=>$category,'head'=>'OVERVIEW','keyword'=>$search]);

    }
    function forumTopic($id){
        $topic=DB::table('forumtopic')->where('TopicId',$id)->leftjoin('forumcat','forumtopic.TopicCat','=','forumcat.CatId')->leftjoin('users','TopicUser','=','id')->select('TopicId','TopicName','TopicDescription','forumtopic.updated_at','CatName','TopicCat','name')->first();
        $comment = DB::table('comment')->leftjoin('users','users.id', '=', 'comment.CommentUser')->where('CommentType','F')->where('CommentRefID',$id)->where('CommentStatus','1')->orderby('comment.updated_at','DESC')->get();
        $cat=$topic->TopicCat;
        $related=DB::table('forumTopic')->where(['TopicStatus'=>'1','TopicCat'=>$cat])->where('TopicId','<>',$id)->orderby('created_at','DESC')->limit(5)->get();
        return view('forum.forumTopic')->with(['topic'=>$topic,'comments'=>$comment,'related'=>$related]);

    }

    function createTopic(Request $request){
        $topic = new forumTopic();
        $topic->TopicName = $request['questionHead'];
        $topic->TopicDescription = $request['questionBody'];
        $topic->TopicCat = $request['category'];
        $topic->TopicUser = Auth::user()->id;
        $topic->save();
        return redirect('forum');
    }
}
