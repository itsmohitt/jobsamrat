<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class jobQues extends Controller
{
    function examPrep(){
        $topicRow=DB::table('quetopic')->where('TopicStatus','1')->get();
        $topics=array();
        foreach($topicRow as $topic){
            $subTopicRow=DB::table('quesubtopic')->where('TopicId',$topic->TopicId)->where('SubTopicStatus',1)->get();
            array_push($topics,array($topic,$subTopicRow));
        }
        return view('ques.category')->with('topics',$topics);
    }
    function queTopic($id){
        $value=DB::table('quesubtopic')->where('SubTopicStatus','1')->where('TopicId',$id)->get();
        return view('ques/topic')->with('subTopics',$value);
    }
    function queSubTopic($id){
        $value=DB::table('jobque')->where(function($query) use ($id){
            $query->where('SubTopicId',$id)
                ->orWhere('SubTopicId','LIKE','%,'.$id)
                ->orWhere('SubTopicId','LIKE','%,'.$id.',%')
                ->orWhere('SubTopicId','LIKE',$id.',%');
        })->where('QueStatus','1')->paginate(8);
        return view('ques/question')->with('questions',$value);
    }

    function queDetail($id){
        $value=DB::table('jobQue')->where('QueId',$id)->first();
        $topics="'".implode("','",explode(',',$value->SubTopicId))."'";
        $subTopics=DB::table('quesubtopic')->whereraw('SubTopicId IN ('.$topics.')')->get();
        $comment = DB::table('comment')->leftjoin('users','users.id', '=', 'comment.CommentUser')->where('CommentRefID',$id)->where('CommentType','Q')->where('CommentStatus','1')->orderby('comment.updated_at','DESC')->get();
        return view('ques/questionDetail')->with('var',array($value,$subTopics,$comment));
    }
}
