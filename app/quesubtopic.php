<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quesubtopic extends Model
{
    protected $table = 'quesubtopic';
    protected $primaryKey = 'SubTopicId';
    protected $fillable = [
        'SubTopicId',
        'TopicId',
        'SubTopicName',

        'SubTopicStatus',
    ];
}
