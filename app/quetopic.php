<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quetopic extends Model
{
    protected $table = 'quetopic';
    protected $primaryKey = 'TopicId';
    protected $fillable = [
        'TopicId',
        'TopicName',
        'TopicStatus',
    ];
}
