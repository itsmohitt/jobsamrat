<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ques extends Model
{
    protected $table        = 'jobque';
    protected $primaryKey   = 'QueId';
    protected $fillable     = [
        'Question',
        'OptionA',
        'OptionB',
        'OptionC',
        'OptionD',
        'OptionE',
        'QueAnswer',
        'SubTopicId',
        'QueDifficulty',

    ];
}
